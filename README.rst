========
sardonic
========

A hardware inventory system

Sardonic is a hardware inventory system; not quite a CMDB, but a high quality
data source for input into a CMDB. It is intended to integrate with Ironic, and
possibly other OpenStack projects in the future.

* Free software: Apache license
* Documentation: https://docs.openstack.org/sardonic/latest
* Source: https://git.openstack.org/cgit/openstack/sardonic
* Bugs: https://storyboard.openstack.org/#!/project/1075
